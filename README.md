# A complete version for 2nd session
>git checkout 2ndSession-Refined

# A done version
>git checkout 2ndSession

# An initial version for the 2nd session
>git checkout 1stSession

# An initial version for the 1st session
>git clone git@gitlab.com:ruvlab/akka-skooldio-part2.git
